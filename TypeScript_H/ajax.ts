namespace H {
    export interface iAjaxFail {
        url: string;
        errorCode: number;
    } 

    export class Ajax {
        private _url : string;
        public get url() : string {
            return this._url;
        }
        public set url(v: string) {
            this._url = v
        }
        
        private _method : "GET"|"POST";
        public get method(): "GET"|"POST" {
            return this._method;
        }
        
        private _self : XMLHttpRequest;
        public get self(): XMLHttpRequest {
            return this._self;
        }
        
        private _onSuccess : (resp: any) => void;
        public get onSuccess() : (resp: any) => void {
            return this._onSuccess;
        }
        public set onSuccess(v : (resp: any) => void) {
            this._onSuccess = v;
        }
        
        private _onFail: (error: iAjaxFail) => void;
        public get onFail(): (error: iAjaxFail) => void {
            return this._onFail;
        }
        public set onFail(v: (error: iAjaxFail) => void) {
            this._onFail = v;
        }
        
        private _isJsonResponse : boolean;
        public get isJsonResponse() : boolean {
            return this._isJsonResponse;
        }
        public set isJsonResponse(v : boolean) {
            this._isJsonResponse = v;
        }
        
        public constructor(method: "GET" | "POST", url: string = "", isJsonResponse: boolean = true) {
            //Variables Locales
            this._isJsonResponse = isJsonResponse
            this._method = method
            this._url = url

            this._onSuccess = (resp) => {}
            this._onFail = () => {}

            //Inicializar
            this._self = new XMLHttpRequest()
            this._self.onreadystatechange = () => {
                if ((this._self.readyState == 4) && (this._self.status == 200)) {
                    //Respuesta correcta y completada
                    this.execOnSuccess()

                } else if (this._self.readyState == 4) {
                    //Respuesta con errores
                    this.execOnFail()

                }

                return
            }
        }

        private execOnSuccess() {
            //Parsear response
            let resp: any = null
            if (this._isJsonResponse) {
                try {
                    resp = JSON.parse(this._self.response)
                
                } catch (ex) {
                    resp = this._self.response
                
                }
            } else {
                resp = this._self.response

            }

            //Ejecutar Callback
            this._onSuccess(resp)
        }

        private execOnFail() {
            //Ejecutar Fallo
            let error: iAjaxFail = {
                url: this._url,
                errorCode: this._self.status
            }
            this._onFail(error)
        }

        public async send(req?: any) {
            this._self.open(this._method, this._url, true)
            this._self.setRequestHeader("Content-Type", "application/json;charset=UTF-8")
            this._self.setRequestHeader("Access-Control-Allow-Origin", "*")
            await this._self.send(JSON.stringify(req))
        }

        public async sendFile(data: File) {
            //Prepare the header
            this._self.open(this._method, this._url, true)
            this._self.setRequestHeader("Content-Type", data.type)
            this._self.setRequestHeader("Access-Control-Allow-Origin", "*")

            //Read the FILES
            let reader = new FileReader()
            let fileBlob = data.slice(0, data.size, data.type)

            //When the file is readed...
            reader.addEventListener("loadend", (e: any) => {
                //Get string representation of the file
                let dataOut: string = ""
                try {
                    dataOut = e.currentTarget.result
                } catch (e) {
                    dataOut = e.srcElement.result
                }

                if (dataOut.trim() != "") {
                    this._self.send(dataOut)
                } else {
                    console.log("[FAIL] - AJAX: El archivo ingresado está vacío.")
                }
            })
            reader.readAsText(fileBlob)
        }
    }
}