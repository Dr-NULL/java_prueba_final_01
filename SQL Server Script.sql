USE master
DROP DATABASE IF EXISTS JavaWeb_PruebaFinal01
CREATE DATABASE JavaWeb_PruebaFinal01
GO

USE JavaWeb_PruebaFinal01
GO

---------------------------------------------------------------------------------------------------------
--CREAR TABLAS-------------------------------------------------------------------------------------------
CREATE TABLE CLIENTE(
	CliRut				NUMERIC(12, 0)			PRIMARY KEY,
	CliNombre			VARCHAR(100)			NOT NULL,
	CliApellido			VARCHAR(100)			NOT NULL,
	CliFechaIngreso		DATE					NOT NULL,
	CliDireccion		NVARCHAR(100)				NULL,
	CliGiro				TINYINT						NULL,
	CliFono				NUMERIC(10, 0)				NULL
)

INSERT INTO CLIENTE VALUES
(0019, 'Test', 'Client', CONVERT(date, '01/01/2000', 103), 'Test Street #1234', 1, 111111111),
(1111, 'Good', 'Tiger', CONVERT(date, '01/01/1995', 103), 'Another Street #1234', 1, 111111112),
(6666, 'Remilia', 'Scarlet', CONVERT(date, '01/01/1800', 103), 'Gensokyo #6666', 1, 666666666),
(0000, 'Brian', 'Carroll', CONVERT(date, '01/01/1991', 103), 'King James #9999', 1, 111111113),
(8888, 'Adam', 'Jones', CONVERT(date, '01/01/1985', 103), 'Fear Inoculum #1234', 1, 111111114)
GO

CREATE TABLE PRODUCTO(
	ProCodigo			NUMERIC(10, 0)			PRIMARY KEY			IDENTITY(1000, 1),
	ProCodigoBarra		VARCHAR(100)				NULL,
	ProNombre			VARCHAR(100)			NOT NULL,
	ProMarca			VARCHAR(50)				NOT NULL,
	ProPrecio			NUMERIC(12, 2)			NOT NULL
)

INSERT INTO PRODUCTO (ProNombre, ProMarca, ProPrecio) VALUES
('Arroz Juan XXII', 'Luchetti', 5990),
('Fideos Molde', 'Banquetisimo', 4500),
('Chocolate Chip Trip', '7empest', 7777),
('Gin Bombai Sapphire', 'Bombai', 110000)
GO

CREATE TABLE TIPO_DCTO(
	VenTipo				TINYINT					PRIMARY KEY			IDENTITY(1, 1),
	VenDescripc			VARCHAR(50)
)

INSERT INTO TIPO_DCTO (VenDescripc) VALUES
('Boleta'),
('Factura')
GO

CREATE TABLE VENTA(
	VenFolio			BIGINT					PRIMARY KEY,
	VenTipo				TINYINT					FOREIGN KEY			REFERENCES TIPO_DCTO(VenTipo),
	CliRut				NUMERIC(12, 0)			FOREIGN KEY			REFERENCES CLIENTE(CliRut),
	VenFecha			DATE					NOT NULL
)
GO

CREATE TABLE DETALLE_VENTA(
	IdDetVenta			BIGINT					PRIMARY KEY			IDENTITY(1, 1),
	VenFolio			BIGINT					FOREIGN KEY			REFERENCES VENTA(VenFolio),
	VenTipo				TINYINT					FOREIGN KEY			REFERENCES TIPO_DCTO(VenTipo),
	ProCodigo			NUMERIC(10, 0)			FOREIGN KEY			REFERENCES PRODUCTO(ProCodigo),
	VenCantidad			INT						NOT NULL,
	VenPrecioReal		NUMERIC(12, 2)			NOT NULL,
	VenDescuento		TINYINT					NOT NULL			DEFAULT 0,
	VenPrecioVenta		NUMERIC(12, 2)
)
GO

CREATE TABLE USUARIO(
	Id					INT						PRIMARY KEY			IDENTITY(1, 1),
	Name				VARCHAR(20)				NOT NULL,
	Email				VARCHAR(100)			NOT NULL,
	Pass				VARCHAR(20)
)

INSERT INTO USUARIO (Name, Email, Pass) VALUES
('Dr-NULL', 'silvaaguilarfelipe@gmail.com', 'admin'),
('Void', 'test_user@gmail.com', '123456789'),
('Mozaiq', 'blood.stain.child@gmail.com', '555')
GO

---------------------------------------------------------------------------------------------------------
--PROCEDIMIENTOS ALMACENADOS-----------------------------------------------------------------------------
CREATE OR ALTER PROCEDURE loginUser
@email		VARCHAR(100),
@pass		VARCHAR(20)

AS
BEGIN
SELECT TOP 1
	Id,
	Name,
	Email

FROM USUARIO

WHERE
		(TRIM(@email) = Email)
	AND	(@pass = Pass) 
END
GO

CREATE OR ALTER PROCEDURE getTipoDcto
AS
BEGIN
SELECT
	VenTipo,
	VenDescripc

FROM TIPO_DCTO

ORDER BY
	VenTipo ASC
END
GO

CREATE OR ALTER PROCEDURE getCliente
AS
BEGIN
SELECT
	CliRut,
	CliNombre,
	CliApellido

FROM CLIENTE

ORDER BY
	CliNombre ASC,
	CliApellido ASC
END
GO

CREATE OR ALTER PROCEDURE getProducto
AS
BEGIN
SELECT
	ProCodigo,
	ProNombre,
	ProMarca,
	ProPrecio

FROM PRODUCTO

ORDER BY
	ProNombre ASC
END
GO

CREATE OR ALTER PROCEDURE getFolio
@venFolio	BIGINT,
@venTipo	TINYINT

AS
BEGIN
SELECT TOP 1
	VENTA.VenFolio,
	TIPO_DCTO.VenDescripc

FROM VENTA

INNER JOIN TIPO_DCTO ON
	VENTA.VenTipo = TIPO_DCTO.VenTipo

WHERE
		(@venFolio = VENTA.VenFolio)
	AND	(@venTipo = VENTA.VenTipo)

ORDER BY
	VenFolio DESC
END
GO

CREATE OR ALTER PROCEDURE addVenta
@venFolio			BIGINT,
@venTipo			TINYINT,
@venFecha			DATE,
@cliRut				NUMERIC(12, 0)

AS
BEGIN
INSERT INTO VENTA VALUES (
	@venFolio,
	@venTipo,
	@cliRut,
	@venFecha
)
END
GO

CREATE OR ALTER PROCEDURE addDetalleVenta
@venFolio			BIGINT,
@venTipo			TINYINT,
@ProCodigo			NUMERIC(10, 0),
@venCantidad		INT,
@venDescuento		TINYINT

AS 
BEGIN
DECLARE @venPrecioReal AS NUMERIC(12, 2) = (
	SELECT
		PRODUCTO.ProPrecio

	FROM PRODUCTO

	WHERE
		@ProCodigo = PRODUCTO.ProCodigo
)
DECLARE @venPrecioVenta AS NUMERIC(12, 2) = ((@venPrecioReal - (@venPrecioReal * (@venDescuento * 0.01))) * @venCantidad)

INSERT INTO DETALLE_VENTA (VenFolio, VenTipo, ProCodigo, VenCantidad, VenPrecioReal, VenDescuento, VenPrecioVenta)
VALUES (@venFolio, @venTipo, @ProCodigo, @venCantidad, @venPrecioReal, @venDescuento, @venPrecioVenta)
END
GO

CREATE OR ALTER PROCEDURE getVentasResumen
AS
BEGIN
SELECT
	VENTA.VenFolio,
	TIPO_DCTO.VenDescripc,
	VENTA.VenFecha,
	CLIENTE.CliRut,
	CONCAT(CLIENTE.CliNombre, ' ', CLIENTE.CliApellido) AS  CliFullName,
	(
		SELECT
			SUM(DETALLE_VENTA.VenPrecioVenta + (DETALLE_VENTA.VenPrecioVenta * 0.19))

		FROM DETALLE_VENTA

		WHERE
				DETALLE_VENTA.VenFolio = VENTA.VenFolio
			AND	DETALLE_VENTA.VenTipo = VENTA.VenTipo

		GROUP BY
			DETALLE_VENTA.VenFolio,
			DETALLE_VENTA.VenTipo

	) AS VenTotal

FROM VENTA

INNER JOIN TIPO_DCTO ON
		VENTA.VenTipo = TIPO_DCTO.VenTipo

INNER JOIN CLIENTE ON
		VENTA.CliRut = CLIENTE.CliRut
END
GO

CREATE OR ALTER PROCEDURE getVentaDetalle
@folio		BIGINT,
@tipoDcto	TINYINT

AS
BEGIN
SELECT
	DETALLE_VENTA.IdDetVenta,
	DETALLE_VENTA.VenCantidad,
	CONCAT(PRODUCTO.ProNombre, ' - ', PRODUCTO.ProMarca) AS ProName,
	DETALLE_VENTA.VenPrecioReal,
	DETALLE_VENTA.VenPrecioVenta

FROM DETALLE_VENTA

INNER JOIN PRODUCTO ON
	DETALLE_VENTA.ProCodigo = PRODUCTO.ProCodigo

WHERE
		@folio = DETALLE_VENTA.VenFolio
	AND	@tipoDcto = DETALLE_VENTA.VenTipo
END
GO

CREATE OR ALTER PROCEDURE remDetalleVenta
@folio		BIGINT,
@tipo		TINYINT,
@id			BIGINT

AS
BEGIN
DELETE FROM DETALLE_VENTA
WHERE
		DETALLE_VENTA.VenFolio = @folio
	AND	DETALLE_VENTA.VenTipo = @tipo
	AND	DETALLE_VENTA.IdDetVenta = @id
END
GO