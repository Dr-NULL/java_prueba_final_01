package cl.aiep.final01.model;

import cl.aiep.final01.connection.*;
import java.util.concurrent.atomic.*;
import java.util.ArrayList;
import java.sql.ResultSet;

public class Cliente {
    private long rut;
    public long getRut() {
        return this.rut;
    }
    
    private String nombre;
    public String getNombre() {
        return this.nombre;
    }
    
    private String apellido;
    public String getApellido() {
        return this.apellido;
    }
    
    private Cliente() {}
    public static ArrayList<Cliente> getAll() {
        AtomicReference<ArrayList<Cliente>> atom = new AtomicReference<>();
        atom.set(new ArrayList<>());
        
        SqlServer sql = new SqlServer();
        sql.exec(new StoredProc(
            "getCliente",
            new Object[]{},
            (ResultSet res) -> {
                ArrayList<Cliente> ref = atom.get();
                Cliente item = new Cliente();
                
                item.rut = res.getLong("CliRut");
                item.nombre = res.getString("CliNombre");
                item.apellido = res.getString("CliApellido");
                
                ref.add(item);
                atom.set(ref);
            }
        ));
        
        return atom.get();
    }
}
