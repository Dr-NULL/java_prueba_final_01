package cl.aiep.final01.model;

import cl.aiep.final01.connection.StoredProc;
import cl.aiep.final01.connection.SqlServer;
import java.util.concurrent.atomic.*;
import java.util.ArrayList;
import java.sql.ResultSet;

public class TipoDcto {
    private int id;
    public int getId() {
        return this.id;
    }
    
    private String descripc;
    public String getDescripc() {
        return this.descripc;
    }
    
    private TipoDcto() {}
    public static ArrayList<TipoDcto> getAll() {
        AtomicReference<ArrayList<TipoDcto>> atom = new AtomicReference<>();
        atom.set(new ArrayList<>());
        
        SqlServer sql = new SqlServer();
        sql.exec(new StoredProc(
            "getTipoDcto",
            new Object[]{},
            (ResultSet res) -> {
                ArrayList<TipoDcto> ref = atom.get();
                TipoDcto item = new TipoDcto();
                
                item.id = res.getInt("VenTipo");
                item.descripc = res.getString("VenDescripc");
                
                ref.add(item);
                atom.set(ref);
            }
        ));
        
        return atom.get();
    }
    
    public static TipoDcto getById(int id) {
        ArrayList<TipoDcto> ref = TipoDcto.getAll();
        TipoDcto item = null;
        
        for (int i = 0; i < ref.size(); i++) {
            if (ref.get(i).getId() == id) {
                item = ref.get(i);
            }
        }
        
        return item;
    }
}
