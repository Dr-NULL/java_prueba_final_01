package cl.aiep.final01.model;

import cl.aiep.final01.connection.*;
import java.util.concurrent.atomic.*;
import java.util.ArrayList;
import java.sql.ResultSet;

public class Detalle {
    private long id;
    public long getId() {
        return this.id;
    }
    
    private int cant;
    public int getCant() {
        return this.cant;
    }
    
    private String prodDescripc;
    public String getProdDescripc() {
        return this.prodDescripc;
    }
    
    private double prodPrecioUnit;
    public double getProdPrecioUnit() {
        return this.prodPrecioUnit;
    }
    
    private double prodPrecioVenta;
    public double getProdPrecioVenta() {
        return this.prodPrecioVenta;
    }
    
    private Detalle() {}
    public static void addNew(long folio, int tipo, long prodCod, int prodCant, int prodDesc) {
        SqlServer sql = new SqlServer();
        sql.exec(new StoredProc(
            "addDetalleVenta",
            new Object[]{
                folio,
                tipo,
                prodCod,
                prodCant,
                prodDesc
            },
            (ResultSet res) -> {}
        ));
    }
    
    public static ArrayList<Detalle> getAll(long folio, int tipo) {
        AtomicReference<ArrayList<Detalle>> atom = new AtomicReference<>();
        atom.set(new ArrayList<>());
        
        SqlServer sql = new SqlServer();
        sql.exec(new StoredProc(
            "getVentaDetalle",
            new Object[]{
                folio,
                tipo
            },
            (ResultSet res) -> {
                ArrayList<Detalle> ref = atom.get();
                Detalle item = new Detalle();
                
                item.id = res.getLong("IdDetVenta");
                item.cant = res.getInt("VenCantidad");
                item.prodDescripc = res.getString("ProName");
                item.prodPrecioUnit = res.getDouble("VenPrecioReal");
                item.prodPrecioVenta = res.getDouble("VenPrecioVenta");
                
                ref.add(item);
                atom.set(ref);
            }
        ));
        
        return atom.get();
    }
    
    public static void remElem(long folio, int tipo, long id) {
        SqlServer sql = new SqlServer();
        sql.exec(new StoredProc(
            "remDetalleVenta",
            new Object[]{
                folio,
                tipo,
                id
            },
            (ResultSet res) -> {}
        ));
    }
}
