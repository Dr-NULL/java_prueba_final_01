package cl.aiep.final01.model;

import cl.aiep.final01.connection.StoredProc;
import cl.aiep.final01.connection.SqlServer;
import java.io.IOException;
import java.util.concurrent.atomic.*;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Usuario {
    private int id;
    public int getId() {
        return this.id;
    }
    
    private String name;
    public String getName() {
        return this.name;
    }
    
    private String email;
    public String getEmail() {
        return this.email;
    }
    
    private String pass;
    public String getPass() {
        return this.pass;
    }
    
    private Usuario() {}
    public static Usuario login(String email, String pass) {
        AtomicReference<Usuario> atom = new AtomicReference<>();
        atom.set(null);
        
        SqlServer sql = new SqlServer();
        sql.exec(new StoredProc(
            "loginUser",
            new Object[] {
                email,
                pass
            },
            (ResultSet res) -> {
                Usuario item = new Usuario();
                
                item.id = res.getInt("Id");
                item.name = res.getString("Name");
                item.email = res.getString("Email");
                item.pass = pass;
                
                atom.set(item);
            }
        ));
        
        return atom.get();
    }
    
    public static void checkSession(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(true);
        if (session.getAttribute("Id") == null) {
            try {
                response.sendRedirect("/UsuarioLogout");
            } catch (IOException ex) {
                Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
