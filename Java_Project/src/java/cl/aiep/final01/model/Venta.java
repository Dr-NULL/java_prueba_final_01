package cl.aiep.final01.model;

import cl.aiep.final01.connection.*;
import java.util.concurrent.atomic.*;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.util.Date;

public class Venta {
    private long folio;
    public long getFolio() {
        return this.folio;
    }
    
    private String tipoDctoDescripc;
    public String getTipoDctoDescripc() {
        return this.tipoDctoDescripc;
    }
    
    private Date fecha;
    public Date getFecha() {
        return this.fecha;
    }
    
    private long cliRut;
    public long getCliRut() {
        return this.cliRut;
    }
    
    private String cliFullName;
    public String getCliFullName() {
        return this.cliFullName;
    }
    
    private double montoTotal;
    public double getMontoTotal() {
        return this.montoTotal;
    }
    
    private Venta() {}
    public static boolean isValid(long folio, int tipoDcto) {
        AtomicReference<Boolean> atom = new AtomicReference<>();
        atom.set(true);
        
        SqlServer sql = new SqlServer();
        sql.exec(new StoredProc(
            "getFolio",
            new Object[]{
                folio,
                tipoDcto
            },
            (ResultSet res) -> {
                atom.set(false);
            }
        ));
        
        return atom.get();
    }
    
    public static void addNew(long folio, int tipoDcto, Date fecha, long rut) {
        SqlServer sql = new SqlServer();
        sql.exec(new StoredProc(
            "addVenta",
            new Object[]{
                folio,
                tipoDcto,
                fecha,
                rut
            }
        ));
    }
    
    public static ArrayList<Venta> getAll() {
        AtomicReference<ArrayList<Venta>> atom = new AtomicReference<>();
        atom.set(new ArrayList<>());
        
        SqlServer sql = new SqlServer();
        sql.exec(new StoredProc(
            "getVentasResumen",
            new Object[]{},
            (ResultSet res) -> {
                ArrayList<Venta> ref = atom.get();
                Venta item = new Venta();
                
                item.folio = res.getLong("VenFolio");
                item.tipoDctoDescripc = res.getString("VenDescripc");
                item.fecha = res.getDate("VenFecha");
                item.cliRut = res.getLong("CliRut");
                item.cliFullName = res.getString("CliFullName");
                item.montoTotal = res.getDouble("VenTotal");
                
                ref.add(item);
                atom.set(ref);
            }
        ));
        
        return atom.get();
    }
}
