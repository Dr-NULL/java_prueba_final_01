package cl.aiep.final01.model;

import cl.aiep.final01.connection.*;
import java.util.concurrent.atomic.*;
import java.util.ArrayList;
import java.sql.ResultSet;

public class Producto {
    private long codigo;
    public long getCodigo() {
        return this.codigo;
    }
    
    private String nombre;
    public String getNombre() {
        return this.nombre;
    }
    
    private String marca;
    public String getMarca() {
        return this.marca;
    }
    
    private double precio;
    public double getPrecio() {
        return this.precio;
    }
    
    private Producto() {}
    public static ArrayList<Producto> getAll() {
        AtomicReference<ArrayList<Producto>> atom = new AtomicReference<>();
        atom.set(new ArrayList<>());
        
        SqlServer sql = new SqlServer();
        sql.exec(new StoredProc(
            "getProducto",
            new Object[]{},
            (ResultSet res) -> {
                ArrayList<Producto> ref = atom.get();
                Producto item = new Producto();
                
                item.codigo = res.getLong("ProCodigo");
                item.nombre = res.getString("ProNombre");
                item.marca = res.getString("ProMarca");
                item.precio = res.getDouble("ProPrecio");
                
                ref.add(item);
                atom.set(ref);
            }
        ));
        
        return atom.get();
    }
}
