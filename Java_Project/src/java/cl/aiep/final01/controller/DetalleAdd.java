package cl.aiep.final01.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cl.aiep.final01.model.Detalle;

public class DetalleAdd extends HttpServlet {
     @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        long folio = Long.parseLong(session.getAttribute("Folio").toString());
        int tipo = Integer.parseInt(session.getAttribute("TipoDcto").toString());
        int cant = Integer.parseInt(request.getParameter("cant").toString());
        long prod = Long.parseLong(request.getParameter("prod").toString());
        int desc = Integer.parseInt(request.getParameter("desc").toString());
        
        Detalle.addNew(folio, tipo, prod, cant, desc);
        response.sendRedirect("/DetalleVenta.jsp");
    }
}
