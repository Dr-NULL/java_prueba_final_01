package cl.aiep.final01.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class UsuarioLogout extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        session.setAttribute("Id", null);
        session.setAttribute("Email", null);
        session.setAttribute("Name", null);
        session.setAttribute("Folio", null);
        session.setAttribute("TipoDcto", null);
        session.invalidate();
        
        response.sendRedirect("/Login.jsp");
    }
}
