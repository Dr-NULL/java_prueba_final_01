package cl.aiep.final01.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

import cl.aiep.final01.model.Venta;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

public class VentaAdd extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        
        //Detectar Folio Inválido
        if (request.getParameter("folio").trim().equals("")) {
            response.sendRedirect("/IngresoVenta.jsp?error=1");
            return;
        }
        
        try {
            //Buscar Folio existente
            long folio = Long.parseLong(request.getParameter("folio"));
            int tipoDcto = Integer.parseInt(request.getParameter("tipoDcto"));
            if (!Venta.isValid(folio, tipoDcto)) {
                response.sendRedirect("/IngresoVenta.jsp?error=2");
                return;
            }

            //Crear Nueva Venta
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            Date fecha = df.parse(request.getParameter("date"));
            long rut = Long.parseLong(request.getParameter("rut"));

            HttpSession session = request.getSession();
            session.setAttribute("Folio", folio);
            session.setAttribute("TipoDcto", tipoDcto);

            Venta.addNew(folio, tipoDcto, fecha, rut);
            response.sendRedirect("/DetalleVenta.jsp");
        } catch (Exception ex) {
            //Errores de Parseo
            Logger.getLogger(VentaAdd.class.getName()).log(Level.SEVERE, null, ex);
            response.sendRedirect("/IngresoVenta.jsp?error=4");
        }
    }
}
