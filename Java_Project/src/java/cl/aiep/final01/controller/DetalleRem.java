package cl.aiep.final01.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cl.aiep.final01.model.Detalle;

public class DetalleRem extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        long folio = Long.parseLong(session.getAttribute("Folio").toString());
        int tipo = Integer.parseInt(session.getAttribute("TipoDcto").toString());
        long id = Long.parseLong(request.getParameter("id"));
        
        Detalle.remElem(folio, tipo, id);
        response.sendRedirect("/DetalleVenta.jsp");
    }
}
