package cl.aiep.final01.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import cl.aiep.final01.model.Usuario;
import javax.servlet.http.HttpSession;

public class UsuarioLogin extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        Usuario user = Usuario.login(
            request.getParameter("email"), 
            request.getParameter("pass")
        );
        
        if (user == null) {
            response.sendRedirect("/Login.jsp?error=1");
        } else {
            HttpSession session = request.getSession(true);
            session.setAttribute("Id", user.getId());
            session.setAttribute("Name", user.getName());
            session.setAttribute("Email", user.getEmail());
            
            response.sendRedirect("/IngresoVenta.jsp");
        }
    }
}