package cl.aiep.final01.connection;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;

public class SqlServer {
    private Connection connection;    
    private String server;
    private int port;
    private String user;
    private String pass;
    private String database;
    
    public SqlServer() {
        //Credentials
        this.server = "BLAMEPC";
        this.port = 1433;
        this.database = "JavaWeb_PruebaFinal01";
        this.user = "sa";
        this.pass = "Clave123";
    }
    
    private void connect() throws ClassNotFoundException, SQLException, Exception {
        String url = "jdbc:sqlserver://";
        url += this.server + ":" + this.port + ";";
        url += "databaseName=" + this.database + ";";
        url += "user=" + this.user + ";";
        url += "password=" + this.pass;
        
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
        this.connection = DriverManager.getConnection(
            url,
            this.user,
            this.pass
        ); 
    }
    
    public void exec(Query query) {
        //Make query
        try {
            System.out.println("SQl Server -- Opened");
            this.connect();
            Statement inst = this.connection.createStatement();
            ResultSet resp = inst.executeQuery(query.text);
            
            //Execute que function every Row collected
            while (resp.next()) {
                query.exec.exec(resp);
            }
            
            //Close Connection
            resp.close();
            this.connection.close();
            System.out.println("SQl Server -- Closed");
        
        } catch (SQLException ex) {
            ex.printStackTrace();
            if (query.fail != null) {
                query.fail.exec(ex);
            }
            return;
        
        } catch (Exception ex) {
            ex.printStackTrace();
            if (query.fail != null) {
                query.fail.exec(ex);
            }
            return;
            
        }
    }
    
    public void exec(StoredProc proc) {
        //Make query
        try {
            //Make Callable String
            String txt = "{call " + proc.name + " (";
            for (int i = 0; i < proc.params.length; i++) {
                if (i != 0) {
                    txt += ", ";
                }
                txt += "?";
            }
            txt += ")}";
            
            //connect
            this.connect();
            CallableStatement callable = this.connection.prepareCall(txt);
            
            //Add Parameters
            for (int i = 0; i < proc.params.length; i++) {
                callable.setObject(i + 1, proc.params[i]);
            }
            
            //Execute Stored Procedure
            try {
                ResultSet resp = callable.executeQuery();
                if (proc.exec != null) {
                    while (resp.next()) {
                        proc.exec.exec(resp);
                    }
                }
                
                resp.close();
            } catch(Exception ex) {
                System.out.println("[FAIL] -> Error no identificado...");
                System.out.println("          Algo pasa con el ResultSet cuando el SP no devuelve resultados.");
                System.out.println("          El problema no es grave y no afecta al funcionamiento.");
                System.out.println("          Su investigación está pendiente...\n");
            }
            
            //Close Connection
            this.connection.close();
        
        } catch (SQLException ex) {
            ex.printStackTrace();
            if (proc.fail != null) {
                proc.fail.exec(ex);
            }
            return;
        
        } catch (Exception ex) {
            ex.printStackTrace();
            if (proc.fail != null) {
                proc.fail.exec(ex);
            }
            return;
            
        }
    }
}
