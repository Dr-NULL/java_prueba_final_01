package cl.aiep.final01.connection;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IExecMethod {
    void exec(ResultSet resp) throws SQLException;
}
