package cl.aiep.final01.connection;

import java.sql.ResultSet;
import java.util.ArrayList;

public class Query {
    protected String text;
    protected IExecMethod exec;
    protected IFailMethod fail;
    
    public Query(String text, IExecMethod exec) {
        this.text = text;
        this.exec = exec;
    }
    
    public Query(String text, IExecMethod exec, IFailMethod fail) {
        this.text = text;
        this.exec = exec;
        this.fail = fail;
    }
}
