package cl.aiep.final01.connection;

public class StoredProc {
    protected String name;
    protected Object[] params;
    protected IExecMethod exec;
    protected IFailMethod fail;
    
    public StoredProc(String name, Object[] params) {
        this.name = name;
        this.params = params;
    }
    
    public StoredProc(String name, Object[] params, IExecMethod exec) {
        this.name = name;
        this.params = params;
        this.exec = exec;
    }
    
    public StoredProc(String text, Object[] params, IExecMethod exec, IFailMethod fail) {
        this.name = text;
        this.params = params;
        this.exec = exec;
        this.fail = fail;
    }
}
