package cl.aiep.final01.connection;

public interface IFailMethod {
    void exec(Exception ex);
}
