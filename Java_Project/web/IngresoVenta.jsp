<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="cl.aiep.final01.model.Usuario"%>
<%@page import="cl.aiep.final01.model.TipoDcto"%>
<%@page import="cl.aiep.final01.model.Cliente"%>
<%@page import="cl.aiep.final01.model.Venta"%>
<% Usuario.checkSession(request, response); 

response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setHeader("Cache-Control","no-cache, no-store, must-revalidate"); //HTTP 1.1
response.setDateHeader("Expires", 0); //prevents caching at the proxy server
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso Ventas</title>
        
        <link rel="stylesheet" type="text/css" href="/Assets/Bootstrap/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/Bootstrap/bootstrap-datepicker.min.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/FontAwesome/css/all.min.css" />
        
        <script src="/Assets/jQuery/jquery.min.js"></script>
        <script src="/Assets/jQuery/popper.min.js"></script>
        <script src="/Assets/jQuery/moment.min.js"></script>
        <script src="/Assets/Bootstrap/bootstrap.min.js"></script>
        <script src="/Assets/Bootstrap/bootstrap-datepicker.min.js"></script>
        <script src="/Assets/Bootstrap/bootstrap-datepicker.es.min.js"></script>


        <style>
            .navbar-brand {
                margin: 0;
                padding: 0;
            }
            
            .navbar-brand i {
                color: #eda136;
                font-size: 2rem;
            }
        </style>
    </head>
    <body>
        <!-- Barra Superior -->
        <nav class="navbar navbar-expand navbar-dark bg-dark">
            <div class="navbar-brand">
                <i class="fab fa-500px"></i>
                <span>Prueba Final</span>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                </ul>
                <div>
                    <a class="btn btn-outline-success" href="/UsuarioLogout">
                        <i class="fas fa-sign-out-alt"></i>
                        <span>Salir</span>
                    </a>
                </div>
            </div>
        </nav>
      
        
        <!-- Main Form -->
        <div class="row justify-content-center">
            <div class="col-8">
                <div class="card mt-4">
                    <div class="card-header">
                        <h5>Ingreso de Venta</h5>
                    </div>
                    <form class="card-body" action="/VentaAdd" method="post">
                        <div class="row">
                            <div class="col-2 form-group">
                                <label>Folio</label>
                                <input name="folio" type="text" class="form-control" placeholder="Ingrese un número" />
                            </div>
                            <div class="col-2 form-group">
                                <label>Tipo Dcto:</label>
                                <select name="tipoDcto" class="form-control">
                            <%
                                ArrayList<TipoDcto> arrTipo = TipoDcto.getAll();

                                for (int i = 0; i < arrTipo.size(); i++) {
                                    TipoDcto item = arrTipo.get(i);
                                    %>
                                    <option value="<%=item.getId() %>"><%= item.getDescripc()%></option>
                                    <%
                                }
                            %>
                                </select>
                            </div>
                            <div class="col-2 form-group">
                                <label>Fecha:</label>
                                <input name="date" class="form-control" readonly />
                            </div>
                            <div class="col-3 form-group">
                                <label>Cliente:</label>
                                <select name="rut" class="form-control">
                            <%
                                ArrayList<Cliente> arrCliente = Cliente.getAll();

                                for (int i = 0; i < arrCliente.size(); i++) {
                                    Cliente item = arrCliente.get(i);
                                    %>
                                    <option value="<%= item.getRut()%>"><%= item.getNombre()%> <%= item.getApellido()%></option>
                                    <%
                                }
                            %>
                                </select>
                            </div>
                            <div class="col-3 form-group">
                                <label><br /></label>
                                <button type="submit" class="btn btn-block btn-primary">
                                    <i class="fas fa-plus"></i>
                                    <span>Agregar Venta</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                                
                <%

                String error = request.getParameter("error");
                if (error != null) {
                    int errCode = Integer.parseInt(error);
                %>

                <!-- Errores -->
                <div class="alert alert-danger mt-4">
                    <h5>ERROR:</h5>
                <%
                if (errCode == 1) {
                %>
                    <p>No se permiten enviar Folios en Blanco!</p>
                <%
                } else if (errCode == 2) {
                %>
                    <p>El folio Ingresado ya existe, por favor elija otro folio disponible.</p>
                <%
                } else if (errCode == 3) {
                %>
                    <p>Usted no tiene ninguna venta en curso, primero cree una antes de continuar.</p>
                <%
                } else if (errCode == 4) {
                %>
                    <p>Ya te ví maldito, así que me estás intentando romper el sistema? :^)</p>
                <%
                }
                %>
                </div>

                <%
                }
                %>

                <!-- Tabla -->
                <div class="card mt-4">
                    <div class="card-header">
                        <h5>Ventas Registradas:</h5>
                    </div>
                    <div class="card-body">
                        <%
                        ArrayList<Venta> arrVenta = Venta.getAll();
                        if (arrVenta.size() > 0) {
                        %>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Folio</th>
                                    <th>Tipo Dcto</th>
                                    <th>Fecha</th>
                                    <th>Rut</th>
                                    <th>Nombre</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                for (int i = 0; i < arrVenta.size(); i++) {
                                    Venta item = arrVenta.get(i);
                                    SimpleDateFormat parser = new SimpleDateFormat("dd/MM/yyyy");
                                %>
                                <tr>
                                    <td><%= item.getFolio() %></td>
                                    <td><%= item.getTipoDctoDescripc()%></td>
                                    <td><%= parser.format(item.getFecha()) %></td>
                                    <td><%= item.getCliRut()%></td>
                                    <td><%= item.getCliFullName()%></td>
                                    <td><%= (long)item.getMontoTotal()%></td>
                                </tr>
                                <%
                                }
                                %>
                            </tbody>
                        </table>
                        <%
                        } else {
                        %>
                        <div class="alert alert-primary mb-0">
                            <p>No hay Elementos que mostrar...</p>
                        </div>
                        <%
                        }
                        %>
                    </div>
                </div>
            </div>
        </div>
                                

        
        <!-- Custom -->
        <script src="IngresoVenta.js"></script>
    </body>
</html>
