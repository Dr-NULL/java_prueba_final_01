"use strict";
var H;
(function (H) {
    class Ajax {
        constructor(method, url = "", isJsonResponse = true) {
            //Variables Locales
            this._isJsonResponse = isJsonResponse;
            this._method = method;
            this._url = url;
            this._onSuccess = (resp) => { };
            this._onFail = () => { };
            //Inicializar
            this._self = new XMLHttpRequest();
            this._self.onreadystatechange = () => {
                if ((this._self.readyState == 4) && (this._self.status == 200)) {
                    //Respuesta correcta y completada
                    this.execOnSuccess();
                }
                else if (this._self.readyState == 4) {
                    //Respuesta con errores
                    this.execOnFail();
                }
                return;
            };
        }
        get url() {
            return this._url;
        }
        set url(v) {
            this._url = v;
        }
        get method() {
            return this._method;
        }
        get self() {
            return this._self;
        }
        get onSuccess() {
            return this._onSuccess;
        }
        set onSuccess(v) {
            this._onSuccess = v;
        }
        get onFail() {
            return this._onFail;
        }
        set onFail(v) {
            this._onFail = v;
        }
        get isJsonResponse() {
            return this._isJsonResponse;
        }
        set isJsonResponse(v) {
            this._isJsonResponse = v;
        }
        execOnSuccess() {
            //Parsear response
            let resp = null;
            if (this._isJsonResponse) {
                try {
                    resp = JSON.parse(this._self.response);
                }
                catch (ex) {
                    resp = this._self.response;
                }
            }
            else {
                resp = this._self.response;
            }
            //Ejecutar Callback
            this._onSuccess(resp);
        }
        execOnFail() {
            //Ejecutar Fallo
            let error = {
                url: this._url,
                errorCode: this._self.status
            };
            this._onFail(error);
        }
        async send(req) {
            this._self.open(this._method, this._url, true);
            this._self.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            this._self.setRequestHeader("Access-Control-Allow-Origin", "*");
            await this._self.send(JSON.stringify(req));
        }
        async sendFile(data) {
            //Prepare the header
            this._self.open(this._method, this._url, true);
            this._self.setRequestHeader("Content-Type", data.type);
            this._self.setRequestHeader("Access-Control-Allow-Origin", "*");
            //Read the FILES
            let reader = new FileReader();
            let fileBlob = data.slice(0, data.size, data.type);
            //When the file is readed...
            reader.addEventListener("loadend", (e) => {
                //Get string representation of the file
                let dataOut = "";
                try {
                    dataOut = e.currentTarget.result;
                }
                catch (e) {
                    dataOut = e.srcElement.result;
                }
                if (dataOut.trim() != "") {
                    this._self.send(dataOut);
                }
                else {
                    console.log("[FAIL] - AJAX: El archivo ingresado está vacío.");
                }
            });
            reader.readAsText(fileBlob);
        }
    }
    H.Ajax = Ajax;
})(H || (H = {}));
//# sourceMappingURL=h.js.map