let getQueryString = (param: string) => {
    let txt = location.search
    txt = txt.replace(/^\?/gi, "")

    let strVal: string = null
    let arr = txt.split("&")

    let reg = new RegExp(
        `^${param}=`,
        "gi"
    )
    arr.forEach(pair => {
        if (pair.match(reg) != null) {
            let patt = pair.match(reg)[0]
            strVal = pair.replace(patt, "")
            strVal = decodeURI(strVal)
            strVal = strVal.trim()

            if (strVal == "") {
                strVal = null
            }
        }
    })

    let out: any = { data: null }
    let value = `{
        "data": %val%
    }`

    try {
        value = value.replace("%val%", strVal)
        out = JSON.parse(value)
    }
    catch (ex) {
        out.data = strVal
    }

    return out.data
}