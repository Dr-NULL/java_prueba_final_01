declare namespace H {
    interface iAjaxFail {
        url: string;
        errorCode: number;
    }
    class Ajax {
        private _url;
        url: string;
        private _method;
        readonly method: "GET" | "POST";
        private _self;
        readonly self: XMLHttpRequest;
        private _onSuccess;
        onSuccess: (resp: any) => void;
        private _onFail;
        onFail: (error: iAjaxFail) => void;
        private _isJsonResponse;
        isJsonResponse: boolean;
        constructor(method: "GET" | "POST", url?: string, isJsonResponse?: boolean);
        private execOnSuccess;
        private execOnFail;
        send(req?: any): Promise<void>;
        sendFile(data: File): Promise<void>;
    }
}
