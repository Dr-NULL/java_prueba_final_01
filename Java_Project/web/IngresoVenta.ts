//Declaraciones
declare function moment(): {
    format(ref: string): string
};

//
namespace IngresoVentas {
    //Datepicker
    let txtDate = $("input[name=date]")
    txtDate.val(moment().format("DD/MM/YYYY"))
    txtDate.datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        todayHighlight: true
    })

    //Folio
    let txtFolio = $("input[name=folio]")
    txtFolio.focus()
    txtFolio.keyup(() => {
        let value = (txtFolio.val() as string).trim()
        let numbers = value.match(/[0-9]/gi)

        if (numbers == null) {
            value = "0"
        } else {
            value = ""
            numbers.forEach(n => {
                value += String(n)
            })
        }

        txtFolio.val(parseInt(value))
    })
}