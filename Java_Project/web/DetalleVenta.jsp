<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="cl.aiep.final01.model.Usuario"%>
<%@page import="cl.aiep.final01.model.TipoDcto"%>
<%@page import="cl.aiep.final01.model.Producto"%>
<%@page import="cl.aiep.final01.model.Detalle"%>
<%@page import="java.util.ArrayList"%>
<% Usuario.checkSession(request, response); 

response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setHeader("Cache-Control","no-cache, no-store, must-revalidate"); //HTTP 1.1
response.setDateHeader("Expires", 0); //prevents caching at the proxy server

long folio = 0;
int tipo = 0;

try {
    folio = Long.parseLong(session.getAttribute("Folio").toString());
    tipo = Integer.parseInt(session.getAttribute("TipoDcto").toString());
} catch(Exception ex) {
    response.sendRedirect("/IngresoVenta.jsp?error=3");
    return;
}
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso Ventas</title>
        
        <link rel="stylesheet" type="text/css" href="/Assets/Bootstrap/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/Bootstrap/bootstrap-datepicker.min.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/FontAwesome/css/all.min.css" />
        
        <script src="/Assets/jQuery/jquery.min.js"></script>
        <script src="/Assets/jQuery/popper.min.js"></script>
        <script src="/Assets/Bootstrap/bootstrap.min.js"></script>
        
        <style>
            .navbar-brand {
                margin: 0;
                padding: 0;
            }
            
            .navbar-brand i {
                color: #eda136;
                font-size: 2rem;
            }
        </style>
    </head>
    <body>
        <!-- Barra Superior -->
        <nav class="navbar navbar-expand navbar-dark bg-dark">
            <div class="navbar-brand">
                <i class="fab fa-500px"></i>
                <span>Prueba Final</span>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                </ul>
                <div>
                    <a class="btn btn-outline-success" href="/UsuarioLogout">
                        <i class="fas fa-sign-out-alt"></i>
                        <span>Salir</span>
                    </a>
                </div>
            </div>
        </nav>
        
        <div class="row justify-content-center">
            <div class="col-10">
                <form class="card mt-4" action="/DetalleAdd" method="post">
                    <div class="card-header">
                        <%
                        String tipoObj = TipoDcto.getById(tipo).getDescripc();
                        %>
                        <h5>Detalle <%= tipoObj%> Nro <%= folio%></h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-1 form-group">
                                <label>Cantidad:</label>
                                <input type="text" name="cant" class="form-control" />
                            </div>
                            <div class="col-4 form-group">
                                <label>Producto:</label>
                                <select name="prod" class="form-control">
                                <%
                                ArrayList<Producto> arrProd = Producto.getAll();
                                
                                for (int i = 0; i < arrProd.size(); i++) {
                                    Producto item = arrProd.get(i);
                                %>
                                    <option data-prec="<%= item.getPrecio() %>" value="<%= item.getCodigo() %>"><%= item.getNombre() %> - <%= item.getMarca() %></option>
                                <%
                                }
                                %>
                                </select>
                            </div>
                            <div class="col-2 form-group">
                                <label>Precio:</label>
                                <input type="text" name="prec" class="form-control" readonly />
                            </div>
                            <div class="col-2 form-group">
                                <label>Descuento (%):</label>
                                <input type="text" name="desc" class="form-control" />
                            </div>
                            <div class="col-2 form-group">
                                <label>Total</label>
                                <input type="text" name="total" class="form-control" readonly />
                            </div>
                            <div class="col-1 form-group">
                                <label><br /></label>
                                <button type="submit" class="btn btn-block btn-primary">
                                    <i class="fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                                
                <div class="card mt-4">
                    <div class="card-header">
                        <h5>Productos Agregados:</h5>
                    </div>
                    <div class="card-body">
                    <%
                    ArrayList<Detalle> arrDet = Detalle.getAll(folio, tipo);
                    long numNeto = 0;
                    long numIva = 0;
                    long numTotal = 0;
                    
                    if (arrDet.size() == 0) {
                    %>
                        <div class="alert alert-primary">
                            <p>Agregue elementos a la compra para que ésta tenga productos asociados.</p>
                        </div>
                    <%
                    } else {
                    %>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center">Cant</th>
                                    <th class="text-center">Producto</th>
                                    <th class="text-center">Prec Unit</th>
                                    <th class="text-center">Total</th>
                                    <th class="text-center">Quitar</th>
                                </tr>
                            </thead>
                            <tbody>
                            <%
                            for (int i = 0; i < arrDet.size(); i++) {
                                Detalle item = arrDet.get(i);
                                
                                numNeto += (long)item.getProdPrecioVenta();
                            %>
                                <tr>
                                    <td class="text-right"><%= item.getCant() %></td>
                                    <td class="text-left"><%= item.getProdDescripc() %></td>
                                    <td class="text-right"><%= (long)item.getProdPrecioUnit() %></td>
                                    <td class="text-right"><%= (long)item.getProdPrecioVenta() %></td>
                                    <td class="text-center">
                                        <a href="/DetalleRem?id=<%= item.getId() %>" class="btn btn-danger">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            <%
                            }

                            numIva = (long)(numNeto * 0.19);
                            numTotal = numNeto + numIva;
                            %>
                            </tbody>
                        </table>
                        <div class="mt-4">
                            <div class="row">
                                <div class="col-3 form-group">
                                    <label>Valor Neto:</label>
                                    <input type="text" id="txtNeto" class="form-control" value="<%= numNeto %>" readonly />
                                </div>
                                <div class="col-3 form-group">
                                    <label>Valor IVA (19%):</label>
                                    <input type="text" id="txtIva" class="form-control" value="<%= numIva %>" readonly />
                                </div>
                                <div class="col-3 form-group">
                                    <label>Total:</label>
                                    <input type="text" id="txtTotal" class="form-control" value="<%= numTotal %>" readonly />
                                </div>
                                <div class="col-3 form-group">
                                    <label><br /></label>
                                    <a href="/DetalleClose" class="btn btn-block btn-success">
                                        <i class="fas fa-thumbs-up"></i>
                                        <span>Finalizar</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <%
                    }
                    %>
                    </div>
                </div>
            </div>
        </div>
                                
        <!-- Custom -->
        <script src="DetalleVenta.js"></script>
    </body>
</html>
