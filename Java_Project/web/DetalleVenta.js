"use strict";
var DetalleVenta;
(function (DetalleVenta) {
    let txtCant = $("input[name=cant]");
    let selProd = $("select[name=prod]");
    let txtPrec = $("input[name=prec]");
    let txtDesc = $("input[name=desc]");
    //Solo dejar números
    let validate = (elem) => {
        let txtvalue = elem.val().trim();
        let numbers = txtvalue.match(/[0-9]/gi);
        if (numbers == null) {
            txtvalue = "0";
        }
        else {
            txtvalue = "";
            numbers.forEach(n => {
                txtvalue += String(n);
            });
        }
        let value = parseInt(txtvalue);
        if (elem.attr("name") == "desc") {
            if (value > 20) {
                value = 20;
            }
            else if (value < 0) {
                value = 0;
            }
            txtDesc.val(value);
        }
        if (elem.attr("name") == "cant") {
            if (value < 1) {
                value = 1;
            }
        }
        elem.val(value);
        calculate();
    };
    //Calcular Total
    let txtTotal = $("input[name=total]");
    let calculate = () => {
        let cant = parseInt(txtCant.val());
        let prec = parseInt(txtPrec.val());
        let desc = parseInt(txtDesc.val());
        let total = prec - (prec * (desc * 0.01));
        total *= cant;
        txtTotal.val(`${total}`.replace(/\..+$/gi, ""));
    };
    calculate();
    //--------------------------------------------------------------------
    //Cantidad
    txtCant.val(1);
    txtCant.keyup(() => {
        validate(txtCant);
    });
    txtCant.focus((me) => {
        txtCant.select();
    });
    txtPrec.val(0);
    //Descuento
    txtDesc.val(0);
    txtDesc.keyup(() => {
        validate(txtDesc);
    });
    txtDesc.focus((me) => {
        txtDesc.select();
    });
    //Precio
    txtPrec.val(0);
    selProd.change(() => {
        let value = selProd.find("option:selected").attr("data-prec");
        txtPrec.val(value.replace(/\..+$/gi, ""));
        validate(txtPrec);
        calculate();
    });
    selProd.change();
    txtCant.focus();
    txtCant.select();
})(DetalleVenta || (DetalleVenta = {}));
//# sourceMappingURL=DetalleVenta.js.map