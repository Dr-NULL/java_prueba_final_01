<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>Login</title>
        
        <link rel="stylesheet" type="text/css" href="/Assets/Bootstrap/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/FontAwesome/css/all.min.css" />
        
        <script src="/Assets/jQuery/jquery.min.js"></script>
        <script src="/Assets/jQuery/popper.min.js"></script>
        <script src="/Assets/Bootstrap/bootstrap.min.js"></script>
        <style>
            body {
                background-image: url("/Assets/IMG/Login_BG.png");
                background-position: top;
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-size: cover;
            }
            
            .login {
                display: flex;
                display: -webkit-flex;
                flex-flow: row nowrap;
                justify-content: center;
                align-items: center;

                position: absolute;
                left: 0;
                top: 0;
                width: 100vw;
                height: 100vh;
                
                background: none;
            }
            
            .login-box {
                background: rgba(0, 0, 0, 0.15);
                padding: 1.5rem;
                border-radius: 0.5rem;
                border: 1px solid rgba(0, 0, 0, 0.2);
            }
            
            .login-title {
                display: flex;
                display: -webkit-flex;
                flex-flow: row nowrap;
                justify-content: center;
                align-items: center;
                
                padding-bottom: 2rem;
            }
            
            .login-title > i {
                font-size: 4rem;
                width: 6rem;
                height: 6rem;
                padding: 1rem;
                
                color: #eda136;
                background: #202020;
                border-radius: 1rem;
            }
            
            .login-title > span {
                font-size: 3rem;
                margin-left: 1rem;
            }
        </style>
    </head>
    <body>
        <div class="row login">
            <form class="col-8 col-sm-6 col-md-5 col-lg-4 col-xl-3" action="/UsuarioLogin" method="post">
                <div class="login-title">
                    <i class="fab fa-500px"></i>
                    <span>Prueba Final</span>
                </div>
                <div class="login-box">
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="Usuario:" />
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="pass" placeholder="Contraseña:" />
                    </div>
                    <button type="submit" class="btn btn-block btn-primary mt-4">
                        <i class="fas fa-sign-in-alt"></i>
                        <span>Login</span>
                    </button>
                </div><%
            String error = request.getParameter("error");
            System.out.println(error);
                    
            if (error != null) {%>
                <div class="alert alert-danger mt-5">
                    <p>Usuario o Contraseña No Válidos, reintente...</p>
                </div>
            <%}%>
            </form>
        </div>
    </body>
</html>
